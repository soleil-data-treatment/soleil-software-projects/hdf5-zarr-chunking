# HDF5 Zarr Chunking

Python scripts to study the impact of changing the chunk shape on the final file size.

# Usage

Here, we assume that the raw data are stored in an EDF file `edf_file`.

## EDF to HDF5
An EDF file can be converted to an HDF5 file using:
```
python3 edf_to_hdf5.py $edf_file -h
```
The chunk shape can be specified using the `-c` option (e.g., (20, 2048, 2048)):
```
python3 edf_to_hdf5.py $edf_file -c 20 2048 2048
```
Frame indices can be chosen using the `-i` option (e.g., 0,2,5-10):
```
python3 edf_to_hdf5.py $edf_file -i 0,2,5-10
```
An output filename can be specified with the `-o` option (e.g., `oufile.hdf5`):
```
python3 edf_to_hdf5.py $edf_file -o outfile.hdf5
```

## EDF to Zarr
Similarly, we convert an EDF file to a Zarr file using:
```
python3 edf_to_zarr.py $edf_file -h
```
Similar options as described in the previous section can used.
