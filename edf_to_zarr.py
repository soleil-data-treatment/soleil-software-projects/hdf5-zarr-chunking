#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from pathlib import Path
import numpy as np

import fabio
from numcodecs import blosc
from numcodecs import Blosc
import zarr

from tools import get_indices

DTYPE = np.uint16  
EXTENSION = '.zarr'


def convert_edf_to_zarr(edf_file, outfile, frame_indices=None, chunks=None, dtype=DTYPE):
    """Read edf file containing many arrays"""
    fd = fabio.open(edf_file)
      
    if not frame_indices:
        frame_indices = range(0, fd.nframes)

    nb_frames = len(frame_indices)
    print("Number of frames: {}".format(nb_frames))
  
    image_shape = fd.getframe(0).shape
    print("Image shape: {}".format(image_shape))
      
    # Creating an HDF5 file without compression for all images in the edf file
    cube_shape = (nb_frames,) + image_shape

    print("outfile: {}".format(outfile))

    if not chunks:
        z1 = zarr.open(outfile, mode='w', shape=cube_shape, chunks=False, dtype=dtype, compressor=None)
    else:
        print("chunks = {}".format(chunks))
        blosc.set_nthreads(8)
        compressor = Blosc(cname='lz4', clevel=1, shuffle=Blosc.BITSHUFFLE)
        z1 = zarr.open(outfile, mode='w', shape=cube_shape, chunks=chunks, dtype=dtype, compressor=compressor)

    for i, frame_id in enumerate(frame_indices):
        frame = fd.getframe(frame_id)
        z1[i,:,:] = frame.data

  
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('edf_file', type=str, help="Input edf file to be converted to a sequence of image files")
    parser.add_argument('-i', '--frame-ids', type=str, default=None, help="List of wanted frame ids (e.g., 1-10,15,20-25) [DEFAULT: all]")
    parser.add_argument('-o', '--output-file', type=str, default=None, help="Name of the output HDF5 file")
    parser.add_argument('-c', '--chunks', type=int, nargs='+', default=None, help="Chunk shape and size (e.g., 1 2048 2048")
    args = parser.parse_args()
      
    outfile = args.output_file
    if not outfile:
        outfile = 'data' / Path(args.edf_file).stem + EXTENSION

    frame_indices = get_indices(args.frame_ids)

    chunks = args.chunks
    if chunks:
        chunks = tuple(args.chunks)

    convert_edf_to_zarr(args.edf_file, outfile, frame_indices=frame_indices, chunks=chunks)
